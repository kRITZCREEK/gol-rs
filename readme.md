## Game of life in Rust

An implementation of Conways Game of Life in Rust, with graphical output through
Piston.

![Demo](/gif/gol-rs.gif)

## Running

After cloning all you should need to do is: `cargo run` with the latest stable
Rust toolchain.

## Controls

You can start or stop evolution by pressing space.

You can add cells by clicking, the red dot indicates where the cell will end up
being placed.

## Parameters

You can change various parameters like speed, and sizes in a constants block at
the top of src/main.rs
