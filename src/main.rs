extern crate glutin_window;
extern crate graphics;
extern crate opengl_graphics;
extern crate piston;

use glutin_window::GlutinWindow as Window;
use opengl_graphics::{GlGraphics, OpenGL};
use piston::event_loop::*;
use piston::input::*;
use piston::window::WindowSettings;

use std::collections::HashMap;
use std::collections::HashSet;

/////////
// CONSTANTS to play with
////////
/// How fast the game is running, fastest = 0.0
const UPDATE_DELTA: f64 = 0.2;
/// How large the starting window is
const WINDOW_DIMENSIONS: [u32; 2] = [800, 800];
/// How large a cell is displayed
const CELL_SIZE: f64 = 10.0;

pub struct App {
    gl: GlGraphics,         // OpenGL drawing backend.
    world: World,           // All the cells our world holds
    accumulated_delta: f64, // Time delta since the last evolution
    mouse_pos: (i32, i32),  // The last cell position the mouse pointed at
    viewport: [u32; 2],     // The width and height of our window
}

type Cell = (i32, i32);
type World = HashSet<Cell>;

fn main() {
    // Change this to OpenGL::V2_1 if not working.
    let opengl = OpenGL::V3_2;

    // Create an Glutin window.
    let mut window: Window = WindowSettings::new("Game of life", WINDOW_DIMENSIONS)
        .opengl(opengl)
        .exit_on_esc(true)
        .build()
        .unwrap();
    let start_world: HashSet<Cell> = {
        let mut start_world = HashSet::new();
        // Comment or uncomment the starting world by adding or remomving a slash in the next line
        /**/
        start_world.extend(blinker(0, -10));
        start_world.extend(blinker(0, -7));
        start_world.extend(blinker(5, -7));
        start_world.extend(blinker(5, -13));

        start_world.extend(blinker(0, 10));
        start_world.extend(blinker(0, 13));
        start_world.extend(blinker(5, 13));
        start_world.extend(blinker(5, -23));
        // */
        start_world
    };

    // Create a new game and run it.
    let mut app = App {
        gl: GlGraphics::new(opengl),
        world: start_world,
        accumulated_delta: 0.0,
        mouse_pos: (0, 0),
        viewport: WINDOW_DIMENSIONS,
    };

    let mut running = true;

    let mut events = Events::new(EventSettings::new());
    while let Some(e) = events.next(&mut window) {
        if let Some(r) = e.render_args() {
            app.render(&r);
        }
        if let Some(u) = e.update_args() {
            if running {
                app.update(&u);
            }
        }

        if let Some(pos) = e.mouse_cursor_args() {
            app.mouse_move(pos);
        }

        if let Some(Button::Keyboard(Key::Space)) = e.press_args() {
            running = !running;
        }
        if let Some(Button::Mouse(MouseButton::Left)) = e.press_args() {
            app.click()
        }
    }
}

impl App {
    fn render(&mut self, args: &RenderArgs) {
        use graphics::*;

        const WHITE: [f32; 4] = [1.0, 1.0, 1.0, 1.0];
        const BLACK: [f32; 4] = [0.0, 0.0, 0.0, 1.0];
        const RED: [f32; 4] = [1.0, 0.0, 0.0, 1.0];

        let positions: Vec<(f64, f64)> = self.world
            .clone()
            .into_iter()
            .map(|(x, y)| (x as f64 * CELL_SIZE, y as f64 * CELL_SIZE))
            .collect();
        let square = rectangle::square(0.0, 0.0, CELL_SIZE);
        let mouse_pos = (
            (self.mouse_pos.0 as f64 * CELL_SIZE),
            (self.mouse_pos.1 as f64 * CELL_SIZE),
        );
        let (x, y) = ((args.width / 2) as f64, (args.height / 2) as f64);
        self.viewport = [args.width, args.height];

        self.gl.draw(args.viewport(), |c, gl| {
            // Clear the screen.
            clear(BLACK, gl);

            for position in positions.into_iter() {
                let transform = c.transform.trans(x, y).trans(position.0, position.1);
                rectangle(WHITE, square, transform, gl);
            }
            let transform = c.transform.trans(x, y).trans(mouse_pos.0, mouse_pos.1);
            rectangle(RED, square, transform, gl);
        });
    }

    fn update(&mut self, args: &UpdateArgs) {
        // Only update the world if enough time has elapsed since the last evolution.
        self.accumulated_delta += args.dt;
        if self.accumulated_delta + args.dt > UPDATE_DELTA {
            self.world = evolve(&self.world);
            self.accumulated_delta = 0.0;
        }
    }

    fn mouse_move(&mut self, pos: [f64; 2]) {
        let x = (pos[0] - (self.viewport[0] / 2) as f64) / CELL_SIZE;
        let y = (pos[1] - (self.viewport[1] / 2) as f64) / CELL_SIZE;
        self.mouse_pos = (x as i32, y as i32);
    }

    fn click(&mut self) {
        self.world.insert(self.mouse_pos);
    }
}

fn evolve(world: &World) -> World {
    let mut neighbours: HashMap<Cell, u32> = HashMap::new();

    for &cell in world {
        for neighbour in get_neighbours(cell).into_iter() {
            neighbours
                .entry(neighbour)
                .and_modify(|v| *v += 1)
                .or_insert(1);
        }
    }

    let mut new_world = HashSet::new();
    for (cell, count) in neighbours.into_iter() {
        if (world.contains(&cell) && count >= 2 && count <= 3)
            || (!world.contains(&cell) && count == 3)
        {
            new_world.insert(cell);
        }
    }
    new_world
}

fn blinker(x: i32, y: i32) -> HashSet<Cell> {
    vec![(x, y - 1), (x, y), (x, y + 1)].into_iter().collect()
}

fn get_neighbours(cell: Cell) -> Vec<Cell> {
    let (x, y) = cell;
    vec![
        (x + 1, y - 1),
        (x + 1, y),
        (x + 1, y + 1),
        (x, y - 1),
        (x, y + 1),
        (x - 1, y - 1),
        (x - 1, y),
        (x - 1, y + 1),
    ]
}
